# mdPlayer

Copyright Eric Bachard  August 2018

- ORIGIN : This is the code used in miniDart to catch videos and streams. It is forked from hansonLGE Player_custom_ffmpeg (https://github.com/hansonLGE/Player_custom_ffmpeg).
- LICENSE : to respect the license, it will be under LGPL v2.1 license (see the LICENSE file in the repo). 
- HISTORY : I'll start from the initial code, and apply all my changes after, to keep the history of my own changes.

Last but not least, this code will probably never compile, nor work, since a lot is missing (cleanup, factorization, simplification, missing features).

Eric Bachard, commit done : december 2018

